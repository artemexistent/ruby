#1
a = true
b = true
c = false
x = -20
y = 60
z = 4
print 'a: ', !(a || b) && (a || !b)
print "\n"
print 'b: ', (z != y) <=> (6 >= y) && a || b && c && x >= 1.5
print "\n"
print 'c: ', (8 - x * 2 <= z) && (x ** 2 != y ** 2) || (z >= 15)
print "\n"
print 'd: ', x.positive? && y.negative? || z >= (x * y - (-y / x)) + -z
print "\n"
print 'e: ', !(a || b && !(c || (!a || b)))
print "\n"
print 'f: ', x ** 2 + y ** 2 >= 1 && x >= 0 && y >= 0
print "\n"
print 'g: ', (a && (c && b != b || a) || c) && b
print "\n\n"

n = 3
m = -6
p = false
q = false
print (n / m + m / n > 3) && (p && q || !p && q)

print "\n\n"
#2
x = 10
if x > -4 && x <= 0
  print 'y: ', ((x - 2.0).abs / (x ** 2.0 * Math.cos(x))) ** (1.0 / 7.0)
elsif x.positive? && x <= 12
  print 'y: ', 1.0 / (Math.tan(x + 1.0 / Math.exp(x)) / Math.sin(x) ** 2) ** (2.0 / 7.0)
elsif x > 12 || x < -4
  print 'y: ', 1.0 / (1.0 + x / (1.0 + x / (1.0 + x)))
end

print "\n\n"
#3
result = 1.0
(1..8).each do |i|
  result += 1.0 / (3 ** i)
end

print '2): ', result

print "\n\n"

num = 13
sum = 0
(1..num).each do |i|
  t = 0
  (1..i).each do |j|
    t += Math.sin(j)
  end
  sum += 1 / t
end

print '4): ', sum

print "\n\n"
#4

def factorial(num)
  num > 1 ? num * factorial(num - 1) : 1
end

# 4

prev = 1.0
sum = 0.0
n = 2
eps = 0.00001
while prev > eps
  prev = (factorial(n - 1).to_f / factorial(n + 1)) ** (n * (n + 1))
  sum += prev
  n += 1
end
print sum, "\n\n"


num = 10
a = 3
prev = 1.0
sum = 0.0
n = 0
eps = 0.00001
while prev > eps
  prev = ((num * Math.log(a)) ** num).to_f / (factorial(n))
  sum += prev
  n += 1
end
x = num
q = 1 + (x * Math.log(a)).to_f + ((x * Math.log(a)) ** 2).to_f / factorial(2) + ((x * Math.log(a)) ** 3).to_f / factorial(3)
print a ** x,' computed: ', sum, ' difference with expected is: ', (sum - a ** x).abs, "\n"
print sum,' computed: ', q, ' difference with expected is: ', (sum - q).abs, "\n\n\n"

prev = 1.0
sum = 0.0
n = 1
eps = 0.00001
while prev > eps
  prev = (factorial(3 * n - 1) * factorial(n + 1)).to_f / (factorial(4 * n) * factorial(2 * n))
  sum += prev
  n += 1
end
print sum, "\n\n"

#5
N = 11.0
C = 7.0
PI = 3.141

def y(x)
  ((x - C) / (x ** (3.0 / 4) + x ** (1.0 / N) * C ** (1.0 / 4))) *
    ((x ** (1.0 / 2) * C ** (1.0 / 4) + N ** (1.0 / 4)) / (x ** (1.0 / 2) + C ** (1.0 / 2))) *
    (x ** (1.0 / 4) * C ** (-1.0 / 4)) / (x ** (1.0 / 2) - 2 * x ** (1.0 / 4) * C ** (1.0 / 4) + C ** (1.0 / N))
end

x = 1.0
func = 0.0
delta = (N - 1) / (N + C)
(0..(N + C)).each do |_i|
  printf "x: #{x}\ny: #{y(x)}\n"
  func += y(x)
  x += delta
end
print func, "\n\n"

def z(x)
  Math.sin(2 * x) ** 2 - Math.cos(PI / 3 - 2 * x) * Math.sin(2 * x - PI / 6) -
    Math.tanh((PI + x) / (x + 1)) ** (2.0 / x)
end

x = PI / N
func = 0.0
delta = (PI - PI / N) / (1.5 * N + C)
while x <= PI
  printf "x: #{x}\nz: #{z(x)}\n"
  func += z(x)
  x += delta
end
print func, "\n\n"

def f(x)
  if x > 2 && x < N
    y(x)
  elsif x > N && x < (2 * N)
    z(x)
  else
    y(x) + z(x)
  end
end

x = 2.0
func = 0.0
delta = (C - 2) / (2 * N)
(0..(2 * N)).each do |_i|
  printf "x: #{x}\nf: #{f(x)}\n"
  func += f(x)
  x += delta
end
print func, "\n\n"
