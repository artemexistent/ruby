x = gets.chomp.to_f
a = gets.chomp.to_f
phi = gets.chomp.to_f
PI = Math.atan2(0.0, -1.0)
Y = 4.1 * (10 ** -1.7) / ((x - PI) * Math.sin(5 * x)) + (Math.tan(x.abs) ** 3 - Math.log10(Math.sqrt(a + phi))) / (Math.exp(1) ** PI)
print(Y)
