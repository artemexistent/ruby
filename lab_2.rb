x = [52, 46, 63, 94, 132, 184, 198, 171, 120, 92, 49, 43, 63, 109, 157, 202, 245, 278, 309, 340, 379, 415, 430, 447, 474, 483, 471, 430, 447, 474, 250, 193, 154, 111, 84]
y = [289, 258, 240, 234, 236, 234, 207, 181, 171, 157, 134, 99, 82, 69, 59, 57, 51, 47, 41, 31, 33, 61, 86, 140, 196, 245, 274, 292, 302, 308, 309, 308, 308, 309, 302]

area = 0
(0..(x.size - 2)).each do |i|
  area += (x[i] + x[i + 1]) * (y[i] - y[i + 1])
end
print area.abs / 2

print "\n\n"

p = 2
t = 64
r = 16
puts ((p**r) * (1 - (p**-t))).round

print "\n\n"

n = '100011111001'
result = 0
pow = n.size - 1
(0..(n.size - 1)).each do |i|
  result += n[i].to_i * (2**pow)
  pow -= 1
end
print result

print "\n\n"

n = 444
result = ''
while n > 1
  result += (n % 2).to_s
  n /= 2
end
result += n.to_s
print result.reverse