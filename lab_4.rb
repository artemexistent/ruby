#1
require 'matrix'

array = [1, 0, -1, 3, 0, -1, 4, 0, -1, 30, 4, -4, 17, 47, 0]

temp1 = Array.new(0)
temp2 = Array.new(0)
temp3 = Array.new(0)

i = 0
while i < 14
  if array[i].to_i.zero?
    temp1.push(i)
  elsif array[i].to_i.negative?
    temp2.push(i)
  elsif array[i].to_i.positive?
    temp3.push(i)
  end
  i += 1
end

print temp1 + temp2 + temp3, "\n"

#2


temp_a = (0..7).map { Array.new 8 }
temp_b = (0..7).map { Array.new 8 }
temp_x = Array.new 8
temp_y = Array.new 8

(0..7).each do |i|
  (0..7).each do |j|
    temp_a[i][j] = rand(10)
    temp_b[i][j] = rand(10)
    temp_a[i][j] = 1 if i == j
  end
  temp_x[i] = rand(10)
  temp_y[i] = rand(10)
end

a = Matrix[*temp_a]
b = Matrix[*temp_b]
x = Vector[*temp_x]
y = Vector[*temp_y]

def task9(matrix, vector)
  matrix = matrix.to_a
  vector = vector.to_a
  temp = Array.new(matrix.size) do |i|
    matrix.size.times.inject(0) { |result, k| result + matrix[i][k] * vector[k] }
  end
  Vector[*temp]
end

print task9(a, x), "\n"
print task9(a, y), "\n"
print task9(b, x), "\n"
print task9(b, y), "\n"

#3


puts 'Input dimension (from 3 to 9): '
n = gets.chomp.to_i
while (n < 3) || (n > 9)
  puts 'Wrong number'
  n = gets.chomp.to_i
end

a = (0..(n - 1)).map { Array.new n }
temp_a = (0..(n - 1)).map { Array.new n }
b = Array.new n
temp_b = Array.new n
x = Array.new n

(0..(n - 1)).each do |i|
  (0..(n - 1)).each do |j|
    a[i][j] = 11.0
    a[i][j] = 2.0 if i == j
    temp_a[i][j] = a[i][j]
  end
  b[i] = i + 1.0
  temp_b[i] = b[i]
end


(1..(n - 1)).each do |k|
  (k..(n - 1)).each do |i|
    coefficient = temp_a[i][k - 1] / temp_a[k - 1][k - 1]
    ((k - 1)..(n - 1)).each do |j|
      temp_a[i][j] -= coefficient * temp_a[k - 1][j]
    end
    temp_b[i] -= coefficient * temp_b[k - 1]
  end
end

x[n - 1] = temp_b[n - 1] / temp_a[n - 1][n - 1]
i = n - 2
while i >= 0
  ((i + 1)..(n - 1)).each do |j|
    temp_b[i] -= x[j] * temp_a[i][j]
  end
  x[i] = temp_b[i] / temp_a[i][i]
  i -= 1
end

puts 'A: '
(0..(n - 1)).each do |i|
  p a[i]
end
puts 'b: '
p b
puts 'result: '
p x