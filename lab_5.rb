#1
def f1(x)
  1.0 / (1 * Math.sqrt(x))
end

def f2(x)
  1.0 / (5 - 3 * Math.cos(x))
end

def prm(left, right, func, eps)
  h = (right - left) / eps
  sum = 0
  (1..eps).each do |i|
    x = left + i * h - h / 2
    sum += f1(x) if func == 1
    sum += f2(x) if func == 2
  end
  h * sum
end

def trp(left, right, func, eps)
  h = (right - left) / eps
  sum_a = f1(left) / 2 if func == 1
  sum_a = f2(left) / 2 if func == 2
  sum_b = f1(right) / 2 if func == 1
  sum_b = f2(right) / 2 if func == 2
  (1..(eps / 2)).each do |i|
    sum_a += f1(left + h * i) if func == 1
    sum_a += f2(left + h * i) if func == 2
    sum_b += f1(right - h * i) if func == 1
    sum_b += f2(right - h * i) if func == 2
  end
  h * (sum_a + sum_b)
end

pi = Math.atan2(0.0, -1.0)

puts 'prm for f1: ', prm(0.2, 2.1, 1, 100)
puts 'prm for f2: ', prm(0.2, 3 / (2 * pi), 2, 100)
puts 'trp for f1: ', trp(0.2, 2.1, 1, 100)
puts 'trp for f2: ', trp(0.2, 3 / (2 * pi), 2, 100)


#2

def range(x, n)
  sum = 0
  if n.zero?
    prev = 1
    i = 0
    while prev > 0.00001
      prev = x**(4 * i + 1) / (4 * i + 1)
      sum += prev
      i += 1
    end
  else
    (0..n).each do |j|
      sum += x**(4 * j + 1) / (4 * j + 1)
    end
  end
  sum
end

puts 'Input n (from 17 to 58 or 0 to count infinitely): '
n = gets.chomp.to_i
if n != 0
  while (n < 17) || (n > 58)
    puts 'Wrong number'
    n = gets.chomp.to_i
  end
end

result = 0
i = 0.1
while i <= 0.8
  result += range(i, n)
  i += 0.05
end

puts 'result: ', result